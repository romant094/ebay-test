import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux'
import { AppDispatch, RootState } from '_redux/store'
import { Actions } from '_redux'
import { StringOrNumberType } from 'types'

export const useAppDispatch = () => useDispatch<AppDispatch>()
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector
export const useToggleToFavorites = () => {
  const dispatch = useAppDispatch()
  return (id: StringOrNumberType): void => {
    dispatch(Actions.toggleBookToFavorites(id))
  }
}
