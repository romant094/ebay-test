import { api } from '../service'

export const getBooksByType = async (type: string) => {
  try {
    return await api(`https://www.googleapis.com/books/v1/volumes?q=${type}`, {
      method: 'GET',
      headers: {
        'content-type': 'application/json',
      },
    })
  } catch (exception) {
    return []
  }
}

