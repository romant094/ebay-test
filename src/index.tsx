import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { App } from 'views'
import * as serviceWorker from './serviceWorker'
import { store } from '_redux'
import './index.css'

const root = document.getElementById('root')

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  root,
)

serviceWorker.unregister()
