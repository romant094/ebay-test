export const getNestedProperty = (object, path) => {
  if (!object || !path.length) {
    return undefined
  }
  if (typeof path === 'string') {
    return object[path]
  }
  const prop = path[0]
  const found = object[prop]
  if (!found) {
    return undefined
  }
  if (path.length === 1) {
    return found
  }
  if (typeof found === 'object') {
    return getNestedProperty(found, path.slice(1))
  } else {
    return undefined
  }
}

export const sliceByIndex = (arr, index, item) => {
  const updated = [...arr]
  if (item) {
    updated.splice(index, 1, item)
  } else {
    updated.splice(index, 1)
  }
  return updated
}

export const sliceByValue = (arr, value) => {
  const index = arr.findIndex(item => item === value)
  return sliceByIndex(arr, index)
}

export const findItemAndIndex = (arr, searchData) => {
  let item, index
  if (typeof searchData === 'object') {
    const [key, value] = Object.entries(searchData)[0]
    index = arr.findIndex(item => item[key] === value)
  } else {
    index = arr.findIndex(item => item === searchData)
  }
  item = arr[index]
  return { item, index }
}

export const insertToTheEnd = (arr, item) => [...arr, item]
export const insertToTheBeginning = (arr, item) => [item, ...arr]
export const insertAfterIndex = (arr, index, item) => ({
  ...arr.slice(0, index),
  item,
  ...arr.slice(index + 1),
})

export const updateObjectByKey = (object, data) => ({ ...object, ...data })
