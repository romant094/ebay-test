export type StringOrNumberType = string | number

export interface IBook {
  title: string,
  authors: Array<string>,
  publisher: string,
  publishedDate: string,
  categories: Array<string>,
  description: string,
  previewLink: string,
  image: string,
  id: StringOrNumberType,
}

export interface IBookReducer {
  list: Array<IBook>,
  favorites: Array<IBook>,
}

export interface IAction {
  type: string,

  [key: string]: any,
}
