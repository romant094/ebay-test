import React from 'react'
import { Header } from 'base-components'
import { BookSearch } from '../book-search'
import { Sidebar } from '../sidebar'
import './App.scss'

export const App = () => {
  return (
    <div className='app'>
      <Header />
      <main>
        <BookSearch />
        <Sidebar />
      </main>
    </div>
  )
}
