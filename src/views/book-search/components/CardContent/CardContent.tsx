import React from 'react'
import { FaRegStar, FaStar } from 'react-icons/fa'
import { Button } from 'base-components'
import { IBook } from 'types'
import './CardContent.scss'

const NO_DESCRIPTION_TEXT = 'No description found'
const NO_TITLE_TEXT = 'No title found'

type BookOmitted = Omit<IBook, 'image' | 'id'>

interface CardContentType extends BookOmitted {
  favorite: boolean,
  onFavoriteClick: () => void
}

export const CardContent = ({
  title = NO_TITLE_TEXT,
  favorite = false,
  authors = [],
  publisher = '',
  publishedDate = '',
  categories = [],
  description = NO_DESCRIPTION_TEXT,
  previewLink = '',
  onFavoriteClick,
}: CardContentType) => {
  const FavoriteIcon = favorite ? FaStar : FaRegStar

  const titleNode = (
    <h5
      className='card__view__title'
      title={title}
    >
      {title}
    </h5>
  )
  const headerNode = (
    <div className='card__view__header'>
      {titleNode}
      <Button
        icon={<FavoriteIcon />}
        onClick={onFavoriteClick}
      />
    </div>
  )
  const getParagraphFromArray = (arr: Array<string>, className?: string) =>
    <p className={className}>{arr.join(', ')}</p>
  const getParagraphFromString = (string: string, className?: string) =>
    <p className={className}>{string}</p>

  const categoriesNode = getParagraphFromArray(categories)
  const descriptionNode = getParagraphFromString(
    description,
    'card__view__description'
  )
  const readMoreNode = (
    <a
      href={previewLink}
      rel='noreferrer noopener'
      target='_blank'
    >
      Read more...
    </a>
  )
  const authorsNode = getParagraphFromArray(authors)
  const publishedDateNode = getParagraphFromString(publishedDate)
  const publisherNode = getParagraphFromString(publisher)

  const mainNode = (
    <div className='card__view__main'>
      {categoriesNode}
      {authorsNode}
      {descriptionNode}
      {readMoreNode}
      {publishedDateNode}
      {publisherNode}
    </div>
  )
  return (
    <div className='card__view'>
      {headerNode}
      {mainNode}
    </div>
  )
}
