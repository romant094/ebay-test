import React, { useEffect, useState } from 'react'
import { Card, Form, Grid, TextInput } from 'base-components'
import { SEARCH_INPUT_NAME, SEARCH_INPUT_PLACEHOLDER } from './constants'
import { Actions } from '_redux'
import { useAppDispatch, useAppSelector, useToggleToFavorites } from 'hooks'
import { StringOrNumberType } from 'types'
import { CardContent } from './components'

const HELPER_SEARCH_TEXT = 'Javascript'
const GRID = 330

export const BookSearch = () => {
  const dispatch = useAppDispatch()
  const {
    list: books,
    favorites,
  } = useAppSelector(state => state.books)
  const favoritesIds: Array<StringOrNumberType> = favorites.map(f => f.id)
  const [bookType, updateBookType] = useState('')
  const [bookTypeToSearch, updateBookTypeToSearch] = useState('')

  useEffect(() => {
    bookTypeToSearch && dispatch(Actions.getBooksRequest(bookTypeToSearch))
  }, [bookTypeToSearch])

  const handleChangeSearchField = ({ value }: { value: string }): void => {
    updateBookType(value)
  }

  const handleSearchBooks = (): void => {
    updateBookTypeToSearch(bookType)
  }

  const onToggleToFavorites = useToggleToFavorites()

  const formNode = (
    <Form onSubmit={handleSearchBooks}>
      <TextInput
        value={bookType}
        autoFocus
        fullWidth
        searchable
        placeholder={SEARCH_INPUT_PLACEHOLDER}
        onChange={handleChangeSearchField}
        name={SEARCH_INPUT_NAME}
      />
    </Form>
  )

  const booksNode = (
    <Grid minItemWidth={GRID}>
      {books.map(book => {
        const content = (
          <CardContent
            title={book.title}
            authors={book.authors}
            publisher={book.publisher}
            publishedDate={book.publishedDate}
            categories={book.categories}
            description={book.description}
            previewLink={book.previewLink}
            favorite={favoritesIds.includes(book.id)}
            onFavoriteClick={() => onToggleToFavorites(book.id)}
          />
        )
        return (
          <Card
            key={book.id}
            content={content}
            imgUrl={book.image}
          />
        )
      })}
    </Grid>
  )

  const onClickHelper = ():void => {
    updateBookType(HELPER_SEARCH_TEXT)
    updateBookTypeToSearch(HELPER_SEARCH_TEXT)
  }

  const helperNode = !bookType && (
    <div className='empty'>
      <p>
        Try searching for a topic, for example
        <a onClick={onClickHelper}>&nbsp;"Javascript"</a>
      </p>
    </div>
  )

  const contentNode = books.length > 0 ? booksNode : helperNode

  return (
    <article className='book--container'>
      <section className='search-params'>
        {formNode}
        {contentNode}
      </section>
    </article>
  )
}
