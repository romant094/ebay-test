import React from 'react'
import { useAppSelector, useToggleToFavorites } from 'hooks'
import { SidebarItem } from './SidebarItem'
import './Sidebar.scss'

const FAVORITES_HEADER_TEXT = 'Favorites'
const NO_FAVORITES_TEXT = 'Still no favorites, add some...'

export const Sidebar = () => {
  const { favorites } = useAppSelector(state => state.books)
  const onToggleToFavorites = useToggleToFavorites()

  const list = favorites.map(item => (
    <SidebarItem
      key={item.id}
      item={item}
      onToggleToFavorites={onToggleToFavorites}
    />
  ))

  return (
    <aside className='favorites'>
      <h3 className='favorites__header'>{FAVORITES_HEADER_TEXT} ({favorites.length})</h3>
      <ul className='favorites__list'>
        {favorites.length > 0
          ? list
          : NO_FAVORITES_TEXT
        }
      </ul>
    </aside>
  )
}
