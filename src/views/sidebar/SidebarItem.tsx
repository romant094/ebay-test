import React from 'react'
import { IBook, StringOrNumberType } from 'types'
import { Button } from 'base-components'
import { IoClose } from 'react-icons/io5'

interface ISidebarItem {
  item: IBook,
  onToggleToFavorites: (id: StringOrNumberType) => void,
}

export const SidebarItem = ({
  item,
  onToggleToFavorites,
}: ISidebarItem) => {
  const titleNode = <strong>{item.title}</strong>
  const buttonNode = (
    <Button
      icon={<IoClose />}
      onClick={() => onToggleToFavorites(item.id)}
    />
  )
  return (
    <li className='favorites__item'>
      {titleNode}
      {buttonNode}
    </li>
  )
}
