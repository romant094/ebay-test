import { getNestedProperty } from 'assets/utils'
import { StringOrNumberType } from 'types'

export class Book {
  image: string | undefined
  title: string
  authors: Array<string>
  publisher: string
  publishedDate: string
  categories: Array<string>
  description: string
  previewLink: string
  id: StringOrNumberType

  constructor (item: object) {
    this.image = getNestedProperty(item, ['volumeInfo', 'imageLinks', 'smallThumbnail'])
    this.title = getNestedProperty(item, ['volumeInfo', 'title'])
    this.authors = getNestedProperty(item, ['volumeInfo', 'authors'])
    this.publisher = getNestedProperty(item, ['volumeInfo', 'publisher'])
    this.publishedDate = getNestedProperty(item, ['volumeInfo', 'publishedDate'])
    this.categories = getNestedProperty(item, ['volumeInfo', 'categories'])
    this.description = getNestedProperty(item, ['volumeInfo', 'description'])
    this.previewLink = getNestedProperty(item, ['volumeInfo', 'previewLink'])
    this.id = getNestedProperty(item, 'id')
  }
}
