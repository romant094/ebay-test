import React, { useEffect, useState } from 'react'
import classNames from 'classnames'
import unloadImage from '../Image/no-image.png'
import { Loader } from '../Loader'
import './Card.scss'

enum CardClasses {
  left = '',
  right = 'reverse',
}

interface ICard {
  content: React.ReactNode,
  imgUrl?: string,
  imagePosition?: keyof typeof CardClasses
}

export const Card = ({
  content,
  imgUrl,
  imagePosition = 'left',
}: ICard) => {
  const [error, setError] = useState(false)
  const [loaded, setLoaded] = useState(false)
  const cardClassesString = classNames('card', CardClasses[imagePosition])

  useEffect(() => {
    if (imgUrl) {
      const img: HTMLImageElement = new window.Image()
      img.onload = () => {
        setLoaded(true)
      }
      img.onerror = () => {
        setError(true)
      }
      img.src = imgUrl
    } else {
      setError(true)
    }
  }, [])

  const imageLoader = !loaded && !error && <Loader />
  const styles:React.CSSProperties = {}
  if (loaded) {
    styles.backgroundImage = `url(${imgUrl})`
  }
  if (error || !imgUrl) {
    styles.backgroundImage = `url(${unloadImage})`
  }

  const image = (
    <div
      className='card__image'
      style={styles}
    >
      {imageLoader}
    </div>
  )

  const contentNode = content && <div className='card__content'>{content}</div>

  return (
    <div className={cardClassesString}>
      {image}
      {contentNode}
    </div>
  )
}
