import React from 'react'

interface IForm {
  onSubmit: () => void,
  children: React.ReactNode,
  className?: string
}

export const Form = ({
  children,
  onSubmit,
  className
}: IForm) => {
  const handleSubmit = (event: React.FormEvent) => {
    event.preventDefault()
    onSubmit && onSubmit()
  }

  return (
    <form
      className={className}
      onSubmit={handleSubmit}
    >
      {children}
    </form>
  )
}
