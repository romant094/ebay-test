import React from 'react'
import './Button.scss'

interface IButton {
  disabled?: boolean,
  icon?: React.ReactNode,
  onClick?: () => void
}

export const Button = ({
  disabled,
  icon,
  onClick,
}: IButton) => {
  const handleClick = (event: React.MouseEvent<HTMLButtonElement>): void => {
    event.stopPropagation()
    onClick && onClick()
  }

  return (
    <button
      className='button'
      disabled={disabled}
      onClick={handleClick}
    >
      {icon}
    </button>
  )
}
