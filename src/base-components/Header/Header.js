import React from 'react'
import './Header.scss'

export const Header = () => {
  return (
    <header className='header'>
      <div className='header__content'>
        <h1>My Good Reads</h1>
      </div>
    </header>
  )
}
