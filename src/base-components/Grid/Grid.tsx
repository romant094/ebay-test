import React from 'react'
import './Grid.scss'

interface IGrid {
  children: React.ReactNode,
  minItemWidth: number,
}

export const Grid = ({ children, minItemWidth }: IGrid) => {
  const style = { gridTemplateColumns: `repeat(auto-fit, minmax(${minItemWidth}px, 1fr))` }

  return (
    <div
      className='grid'
      style={style}
    >
      {React.Children.map(children, child => child)}
    </div>
  )
}
