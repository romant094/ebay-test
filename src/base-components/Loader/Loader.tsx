import React from 'react'
import './Loader.scss'
import classNames from 'classnames'

enum Dimensions {
  small = 40,
  medium = 60,
  big = 80
}

interface ILoader {
  size?: keyof typeof Dimensions
}

// Loader was taken from https://loading.io/css/ as is.
export const Loader = ({
  size = 'medium',
}: ILoader) => <div className={classNames('lds-dual-ring', size)}/>
