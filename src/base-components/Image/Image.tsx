import React, { useEffect, useState } from 'react'
import classNames from 'classnames'
import { Loader } from '../Loader'
import unloadedSrc from './no-image.png'
import './Image.scss'

const NO_IMAGE_TEXT = 'No image'

interface IImage {
  src: string,
  alt?: string,
  className?: string,
  style?: any,
}

declare const imageType:typeof Image;

interface Window {
  Image: typeof imageType;
}

export const LazyImage = ({
  src,
  alt = '',
  className,
  style = {},
}: IImage) => {
  const [error, setError] = useState(false)
  const [loaded, setLoaded] = useState(false)
  const classString = classNames('image', className)

  useEffect(() => {
    const img: HTMLImageElement = new window.Image()
    img.onload = () => {
      setLoaded(true)
    }
    img.onerror = () => {
      setError(true)
    }
    img.src = src
  }, [])

  const renderImage = (src: string, alt: string) => {
    return (
      <img
        className={classString}
        style={style}
        src={src}
        alt={alt}
      />
    )
  }

  if (error) {
    return renderImage(unloadedSrc, NO_IMAGE_TEXT)
  }
  if (loaded) {
    return renderImage(src, alt)
  }

  return (
    <div
      className='image-loader'
      style={style}
    >
      <Loader />
    </div>
  )
}
