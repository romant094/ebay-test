import React from 'react'

interface ICode {
  content: any,
  spaceCount?: number,
}

export const Code = ({
  content,
  spaceCount = 2,
}: ICode) => {
  const data = JSON.stringify(content, null, spaceCount)
  return <pre>{data}</pre>
}
