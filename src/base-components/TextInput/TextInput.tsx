import React from 'react'
import classNames from 'classnames'
import './Input.scss'

type OnChangeType = {
  name: string | undefined,
  value: string
}

interface IInput {
  readonly value?: string,
  autoFocus?: boolean,
  fullWidth?: boolean,
  searchable?: boolean,
  name?: string | undefined,
  placeholder?: string,
  onChange?: ({ name, value }: OnChangeType) => void,
  className?: string,
}

export const TextInput = ({
  value,
  autoFocus = false,
  name,
  placeholder,
  onChange,
  className,
  fullWidth = false,
  searchable
}: IInput) => {
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target
    onChange && onChange({ name, value })
  }

  const classString = classNames('text-input', className, { 'full-width': fullWidth })

  return (
    <input
      className={classString}
      autoFocus={autoFocus}
      name={name}
      type={searchable ? 'search' : 'text'}
      value={value}
      placeholder={placeholder}
      onChange={handleChange}
    />
  )
}
