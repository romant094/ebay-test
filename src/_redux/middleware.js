import { Types } from './actions'

export const saveFavoritesToLocalStorage = storeAPI => next => action => {
  const result = next(action)
  if (action.type === Types.TOGGLE_BOOK_TO_FAVORITES) {
    const { books: { favorites } } = storeAPI.getState()
    localStorage.setItem('favorites', JSON.stringify(favorites))
  }
  return result
}
