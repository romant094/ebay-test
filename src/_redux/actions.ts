import { createActions } from 'reduxsauce'

export const {
  Types,
  Creators: Actions,
} = createActions({
  getBooksRequest: ['request'],
  getBooksSuccess: ['list'],
  getBooksFailure: ['error'],
  toggleBookToFavorites: ['id'],
})

console.log(Types)
