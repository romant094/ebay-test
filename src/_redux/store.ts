import { applyMiddleware, combineReducers, createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import createSagaMiddleware from 'redux-saga'
import { all } from 'redux-saga/effects'
import { booksReducer } from './reducer'
import { booksSaga } from './sagas'
import { saveFavoritesToLocalStorage } from './middleware'

// Root saga and root reducer should be defined in a different place.
// But in current project it could be done here because we don't have
// many views, sagas and reducers. In future this could be improved
// or changed.
function * rootSaga () {
  yield all([
    booksSaga(),
  ])
}

const rootReducer = combineReducers({
  books: booksReducer,
})

const sagaMiddleware = createSagaMiddleware()
const middlewaresArr = [saveFavoritesToLocalStorage, sagaMiddleware]

const middleware = process.env.NODE_ENV === 'development'
  ? composeWithDevTools(applyMiddleware(...middlewaresArr))
  : applyMiddleware(...middlewaresArr)

export const store = createStore(rootReducer, middleware)
sagaMiddleware.run(rootSaga)

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
