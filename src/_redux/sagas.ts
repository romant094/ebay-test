import { call, takeEvery, put } from 'redux-saga/effects'
import { getBooksByType } from '../apis'
import { Book } from '../models'
import { Actions, Types } from './actions'
import { IAction } from '../types'

function * getBooks (action: IAction) {
  const { getBooksSuccess, getBooksFailure } = Actions
  try {
    const { items: books } = yield call(getBooksByType, action.request)
    const data = books.map((book: object) => new Book(book))
    yield put(getBooksSuccess(data))
  } catch (error) {
    yield put(getBooksFailure([], error))
  }
}

function * booksSaga () {
  yield takeEvery(
    Types.GET_BOOKS_REQUEST,
    getBooks
  )
}

export { booksSaga }
