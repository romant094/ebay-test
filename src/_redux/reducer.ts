import { createReducer } from 'reduxsauce'
import { IAction, IBookReducer } from 'types'
import { insertToTheEnd } from 'assets/utils'
import { Types } from './actions'
import { findItemAndIndex, sliceByIndex } from '../assets/utils'

const favoritesFromLocalStorage = localStorage.getItem('favorites') || ''
const favorites = JSON.parse(favoritesFromLocalStorage) || []
const INITIAL_STATE: IBookReducer = {
  list: [],
  favorites,
}

const updateDataInState = (state: IBookReducer, data: object): IBookReducer => ({ ...state, ...data })

const getDataSuccess = (
  state: IBookReducer = INITIAL_STATE,
  action: IAction,
): IBookReducer => {
  const { list } = action
  return updateDataInState(state, { list })
}
const toggleBookToFavorites = (
  state: IBookReducer = INITIAL_STATE,
  action: IAction,
): IBookReducer => {
  const { favorites, list } = state
  const { id } = action
  const { item: book } = findItemAndIndex(list, { id })
  const { index: favoritesIndex } = findItemAndIndex(favorites.map(item => item.id), id)
  const data = favoritesIndex === -1
    ? insertToTheEnd(favorites, book)
    : sliceByIndex(favorites, favoritesIndex)
  return updateDataInState(state, { favorites: data })
}

const HANDLERS = {
  [Types.GET_BOOKS_SUCCESS]: getDataSuccess,
  [Types.TOGGLE_BOOK_TO_FAVORITES]: toggleBookToFavorites,
}

export const booksReducer = createReducer(INITIAL_STATE, HANDLERS)
